package ee.uptime.bacchus;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BacchusApplicationTests {

    @Test
    public void contextLoads() {
    }

}
