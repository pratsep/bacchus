create sequence hibernate_sequence start with 1;
create table bid(
 id bigint primary key,
 productid varchar(128),
 fullname varchar(64),
 amount float,
 date datetime
);