'use strict';

App.factory('AuctionItemService', ['$http', '$q', function($http, $q){

    var POST = $http.post;
    var GET = $http.get;

    return {
        post: post,
        get: get,
        fetchAllAuctionItems: fetchAllAuctionItems
    };

    function get(path) {
        return httpCommon(GET, path);
    }

    function post(path, data) {
        return httpCommon(POST, path, data);
    }

    function httpCommon(method, path, data) {
        var url = path;

        var deferred = $q.defer();

        method(url, data).then(
            function(response) {
                deferred.resolve(response.data);
            },
            function(response, status) {
                deferred.reject(response);
            }
        );

        return deferred.promise;
    }


    function fetchAllAuctionItems() {
        return $http.get('http://localhost:8080/api/auction')
            .then(
                function(response){
                    // console.log(response.data[0]["biddingEndDate"] - Date.now());
                    // response.data.map(function(a) {
                    //     var t = a.biddingEndDate.split(/[-:TZ]/);
                    //     var u = new Date(Date.UTC(t[0], t[1]-1, t[2], t[3], t[4], t[5]));
                    //     a.newBiddingEndDate = u;
                    //     a.timeNow = new Date;
                    //     a.timeDiff = a.newBiddingEndDate - a.timeNow;
                    //
                    //     return u;
                    // });

                    // console.log(timeList);

                    console.log(response.data);
                    return response.data;
                },
                function(errResponse){
                    console.error('Error while fetching items');
                    return $q.reject(errResponse);
                }
            );
    }
}]);