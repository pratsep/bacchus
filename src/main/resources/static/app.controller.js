'use strict';

App.controller('AppController', ['$scope', '$timeout', 'AuctionItemService', function($scope, $timeout, AuctionItemService) {
    console.log("Controller");
    var self = this;
    self.items = [];
    self.itemsToShow = [];
    self.dropdownItems = [];
    self.stateParam = "All";
    self.newBid = {};

    self.modalShow=false;
    self.submitted=false;
    self.responseMessage = null;


    $scope.Math = window.Math;



    self.fetchAllItems = function(state){
        AuctionItemService.fetchAllAuctionItems().then(
            function(i) {
                // self.refreshTime = null;
                self.refreshTime = i[0]["timeDiff"];
                self.stateParam = state;

                if (state === 'All') {
                    self.itemsToShow = i;
                } else {
                    var itemList = [];
                    i.forEach(function myFunction(item) {
                        if (item.productCategory === state) {
                            itemList.push(item);
                        }
                    });
                    if (itemList.length > 0) {
                        self.itemsToShow = itemList;
                    } else {
                        self.stateParam = 'All';
                        self.itemsToShow = i;
                    }

                }

                self.items = i;
                self.categories = [];
                i.forEach(function myFunction(item) {
                    if (self.refreshTime > item.timeDiff) {
                        self.refreshTime = item.timeDiff;
                    }

                    if (!self.categories.includes(item.productCategory)) {
                        self.categories.push(item.productCategory);
                    }
                });
                $timeout(function(){
                    if (self.refreshTime != null) {
                        self.refreshTime = null;
                        self.fetchAllItems(self.stateParam);
                    }
                }, (self.refreshTime + 3000));
            },
            function(errResponse){
                console.error('Error while fetching items');
            }
        );
    };



    self.fillDropdown = function(dropdownCategory){
        // self.fetchAllItems(self.stateParam);
        var itemList = [];
        self.items.forEach(function myFunction(item) {
            if (item.productCategory === dropdownCategory) {
                itemList.push(item);
            }
        });
        self.dropdownItems = itemList;
    };

    self.selectItemToBidOn = function(item){
        self.newBid.productid = item.productId;
        self.newBid.bidenddate = item.biddingEndDate;
        self.modalShow = true;
    };


    self.submitBid = function() {
        AuctionItemService.post('http://localhost:8080/api/bid', self.newBid).then(function (response) {
            self.submitted = true;
            if (response.inTime && response.fieldsOk) {
                self.newBid = {};
                self.responseMessage = "Bid successful";
            } else if (!response.inTime) {
                self.newBid = {};
                self.responseMessage = "Bidding time has already ended for that item";
            } else if (response.inTime && !response.fieldsOk) {
                self.responseMessage = "Full name or amount not correctly entered";
                self.retry = true;
            }
        });
    };

    self.okay = function() {
        if (self.retry) {
            self.retry = false;
            self.submitted = false;
            self.responseMessage = null;
        } else {
            self.modalShow = false;
            self.responseMessage = null;
            self.submitted = false;
            self.fetchAllItems(self.stateParam);
        }
    };

    self.cancelBid = function(){
        self.newBid = {};
        self.modalShow = false;
        self.fetchAllItems(self.stateParam);
    };


    self.fetchAllItems(self.stateParam);

}]);
