package ee.uptime.bacchus.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Bid {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String productid;
    private String fullname;
    private Double amount;
    private Date date;
    @Transient
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Date bidenddate;


    public Bid() {
    }

    public Date getBidenddate() {
        return bidenddate;
    }

    public void setBidenddate(Date bidenddate) {
        this.bidenddate = bidenddate;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Bid{" +
                "id=" + id +
                ", productid='" + productid + '\'' +
                ", fullname='" + fullname + '\'' +
                ", amount=" + amount +
                ", date=" + date +
                ", bidenddate=" + bidenddate +
                '}';
    }
}