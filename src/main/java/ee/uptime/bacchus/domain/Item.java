package ee.uptime.bacchus.domain;

import java.util.Date;

public class Item {

    private String productId;
    private String productName;
    private String productDescription;
    private String productCategory;
    private Date biddingEndDate;
    private Long timeDiff;


    public Item() {
    }

    public Item(String productId, String productName, String productDescription, String productCategory, Date biddingEndDate, Long timeDiff) {
        this.productId = productId;
        this.productName = productName;
        this.productDescription = productDescription;
        this.productCategory = productCategory;
        this.biddingEndDate = biddingEndDate;
        this.timeDiff = timeDiff;
    }

    public Long getTimeDiff() {
        return timeDiff;
    }

    public void setTimeDiff(Long timeDiff) {
        this.timeDiff = timeDiff;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public Date getBiddingEndDate() {
        return biddingEndDate;
    }

    public void setBiddingEndDate(Date biddingEndDate) {
        this.biddingEndDate = biddingEndDate;
    }

    @Override
    public String toString() {
        return "Item{" +
                "productId='" + productId + '\'' +
                ", productName='" + productName + '\'' +
                ", productDescription='" + productDescription + '\'' +
                ", productCategory='" + productCategory + '\'' +
                ", biddingEndDate=" + biddingEndDate +
                ", timeDiff=" + timeDiff +
                '}';
    }
}
