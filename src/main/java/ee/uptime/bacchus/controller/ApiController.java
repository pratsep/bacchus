package ee.uptime.bacchus.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ee.uptime.bacchus.domain.Bid;
import ee.uptime.bacchus.domain.Item;
import ee.uptime.bacchus.repository.BidRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ApiController {

   @Autowired
   private BidRepository bidRepository;

   @GetMapping("/bids")
   public List<Bid> getAllBids() {
       List<Bid> bidList = bidRepository.findAll();
       return bidList;
   }

   @CrossOrigin(origins = "*")
   @GetMapping("/auction")
   public List getAllAuctionItems() {
      final String uri = "http://uptime-auction-api.azurewebsites.net/api/Auction";

      RestTemplate restTemplate = new RestTemplate();
      List resultList = restTemplate.getForObject(uri, List.class);

      ObjectMapper mapper = new ObjectMapper();

      List<Item> newList = new ArrayList<>();
      for (Object result:resultList) {
         Item item = mapper.convertValue(result, Item.class);
         Date now = new Date();
         item.setTimeDiff(item.getBiddingEndDate().getTime()-now.getTime());
         newList.add(item);
      }

      return newList;
   }

   @PostMapping("/bid")
   public String addBid(@RequestBody Bid bid) {
      bid.setDate(new Date());
      if ((bid.getBidenddate().getTime() - bid.getDate().getTime()) > 0 && bid.getAmount() != null && bid.getFullname() != null) {
         bidRepository.save(bid);
         return "{\"inTime\": true, \"fieldsOk\": true}";
      } else if((bid.getBidenddate().getTime() - bid.getDate().getTime()) > 0 && (bid.getAmount() == null || bid.getFullname() == null)) {
         return "{\"inTime\": true, \"fieldsOk\": false}";
      } else if((bid.getBidenddate().getTime() - bid.getDate().getTime()) <= 0 && bid.getAmount() != null && bid.getFullname() != null) {
         return "{\"inTime\": false, \"fieldsOk\": true}";
      }
      return "{\"inTime\": false,  \"fieldsOk\": false}";
   }

}