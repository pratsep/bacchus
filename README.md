# BACCHUS

To test the application clone the project, using terminal in project folder run "gradle bootRun".

Application uses in-memory database, so saved data will be lost after shutdown.

Embeddes server runs on port 8080 and main page is available from root path "/". All bids are available
through rest api at "/api/bids"